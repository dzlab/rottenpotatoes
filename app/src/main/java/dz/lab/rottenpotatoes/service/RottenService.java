package dz.lab.rottenpotatoes.service;


import java.util.List;

import dz.lab.rottenpotatoes.model.Movie;
import dz.lab.rottenpotatoes.model.Movies;
import retrofit.Callback;
import retrofit.http.GET;

public interface RottenService {

    @GET("/lists/movies/box_office.json")
    void getMovies(Callback<Movies> callback);
}
