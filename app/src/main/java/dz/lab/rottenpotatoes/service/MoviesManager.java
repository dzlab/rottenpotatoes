package dz.lab.rottenpotatoes.service;

import android.util.Log;

import com.squareup.otto.Bus;
import com.squareup.otto.Produce;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Provides;
import dz.lab.rottenpotatoes.event.MovieEvent;
import dz.lab.rottenpotatoes.model.Movie;
import dz.lab.rottenpotatoes.model.Movies;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

@Singleton
public class MoviesManager {
    private final static String TAG = "MoviesManager";

    final Bus bus;
    final RottenService rottenService;
    List<Movie> all;

    @Inject
    MoviesManager(Bus bus, RottenService rottenService) {
        this.bus = bus;
        this.rottenService = rottenService;
        all = new ArrayList<Movie>();
        Callback<Movies> callback = new Callback<Movies>(){
            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "Failed to retrieve movies data: "+error);
            }
            @Override
            public void success(Movies movies, Response response) {
                Log.d(TAG, "Received data for "+movies.getMovies().size()+" movies");
                MoviesManager.this.bus.post(produceRottenEvent(movies));
            }
        };
        rottenService.getMovies(callback);
    }

    public List<Movie> getAll() {
        return all;
    }

    public Movie getMovie(int id) {
        return all.get(id);
    }

    @Produce
    public MovieEvent produceRottenEvent(Movies movies){
        all = movies.getMovies();
        return new MovieEvent(this);
    }

    @dagger.Module(injects = MoviesManager.class, library = true, complete = false)
    public static class Module {
        @Provides @Singleton
        Executor provideMessagePollThread() {
            return Executors.newSingleThreadExecutor();
        }
    }
}
