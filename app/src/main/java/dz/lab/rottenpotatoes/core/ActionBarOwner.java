package dz.lab.rottenpotatoes.core;

import android.content.Context;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import mortar.Mortar;
import mortar.MortarScope;
import mortar.Presenter;
import rx.functions.Action0;

/**
 * Allows shared configuration of the Android ActionBar.
 */
public class ActionBarOwner extends Presenter<ActionBarOwner.View> {

    private Config config;
    public ActionBarOwner(){}

    @Override
    public void onLoad(Bundle savedInstanceState) {
        if(config != null) update();
    }

    public void setConfig(Config config) {
        this.config = config;
        update();
    }

    public Config getConfig() {
        return config;
    }

    @Override
    protected MortarScope extractScope(View view) {
        return Mortar.getScope(view.getMortarContext());
    }

    private void update() {
        View view = getView();
        if(view == null) return;
        view.setShowHomeEnabled(config.showHomeEnabled);
        view.setUpButtonEnabled(config.upButtonEnabled);
        view.setTitle(config.title);
        view.setMenu(config.actions);
    }

    public interface View {
        void setShowHomeEnabled(boolean enabled);
        void setUpButtonEnabled(boolean enabled);
        void setTitle(CharSequence title);
        void setMenu(List<MenuAction> actions);
        Context getMortarContext();
    }

    public static class MenuAction {
        public final CharSequence title;
        public final int icon;
        public final Action0 action;

        public MenuAction(CharSequence title, Action0 action) {
            this(title, -1, action);
        }
        public MenuAction(CharSequence title, int icon, Action0 action) {
            this.title = title;
            this.action = action;
            this.icon = icon;
        }
    }

    public static class Config {
        public final boolean showHomeEnabled;
        public final boolean upButtonEnabled;
        public final CharSequence title;
        public final List<MenuAction> actions;

        public Config(boolean showHomeEnabled, boolean upButtonEnabled, CharSequence title, List<MenuAction> actions) {
            this.showHomeEnabled = showHomeEnabled;
            this.upButtonEnabled = upButtonEnabled;
            this.title = title;
            this.actions = actions;
        }

        public Config withAction(List<MenuAction> actions) {
            Config config = new Config(showHomeEnabled, upButtonEnabled, title, actions);
            return config;
        }

        public void addMenuAction(MenuAction action) {
            this.actions.add(action);
        }
    }
}
