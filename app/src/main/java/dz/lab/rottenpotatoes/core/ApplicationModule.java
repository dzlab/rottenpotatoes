package dz.lab.rottenpotatoes.core;

import android.content.Context;
import android.util.Log;

import com.squareup.okhttp.Cache;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Bus;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dz.lab.rottenpotatoes.RPApplication;
import dz.lab.rottenpotatoes.event.MainThreadBus;
import dz.lab.rottenpotatoes.service.MoviesManager;
import dz.lab.rottenpotatoes.service.RottenService;
import dz.lab.rottenpotatoes.util.ImageChooser;
import dz.lab.rottenpotatoes.util.Utils;
import flow.Parcer;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

@Module(includes = MoviesManager.Module.class, library = true)
public class ApplicationModule {

    private static final String TAG = "ApplicationModule";

    private final RPApplication application;

    public ApplicationModule(RPApplication application) {
        this.application = application;
    }

    @Provides @Singleton
    Gson provideGson() {
        return new GsonBuilder().create();
    }

    @Provides @Singleton
    Parcer<Object> provideParcer(Gson gson) {
        return new GsonParcer(gson);
    }

    @Provides @Singleton
    Bus provideBus() { return new MainThreadBus(); }

    /**
     * Allow the application context to be injected but require that it be annotated with
     * {@link ForApplication @Annotation} to explicitly differentiate it from an activity context.
     */
    @Provides @Singleton @ForApplication
    Context provideApplicationContext() {
        return application;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        // Create an HTTP client that uses a cache on the file system. Android applications should use
        // their Context to get a cache directory.
        OkHttpClient okHttpClient = new OkHttpClient();
        try {
            File cacheDir = new File(System.getProperty("java.io.tmpdir"), UUID.randomUUID().toString());
            Cache cache = new Cache(cacheDir, 1024);
            okHttpClient.setCache(cache);
        }catch (IOException e) {
            Log.d(TAG, "Failed to configuring caching of http responses: "+e.getMessage());
        }
        return okHttpClient;
    }

    @Provides @Singleton
    RottenService provideReviewService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://xebiamobiletest.herokuapp.com/api/public/v1.0/")
                .setConverter(new GsonConverter(new Gson()))
                // caching as in https://gist.github.com/swankjesse/5889518
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Accept", "application/json;versions=1");
                        if (Utils.isNetworkAvailable(application)) {
                            int maxAge = 60 * 60; // read from cache for 1 hour
                            request.addHeader("Cache-Control", "public, max-age=" + maxAge);
                        } else {
                            int maxStale = 60 * 60 * 24 * 28; // tolerate 4-weeks stale
                            request.addHeader("Cache-Control",
                                    "public, only-if-cached, max-stale=" + maxStale);
                        }
                    }
                })
                .setClient(new OkClient(provideOkHttpClient()))
                .build();
        return restAdapter.create(RottenService.class);
    }
}
