package dz.lab.rottenpotatoes.core;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dz.lab.rottenpotatoes.MainActivity;

/**
 * Created by Bachir on 02/11/2014.
 */
@Module(injects = MainActivity.class)
public class ActionBarModule {
    @Provides @Singleton ActionBarOwner provideActionBarOwner() {
        return new ActionBarOwner();
    }
}
