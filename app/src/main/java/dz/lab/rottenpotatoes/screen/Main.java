package dz.lab.rottenpotatoes.screen;

import android.os.Bundle;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Provides;
import dz.lab.rottenpotatoes.MainActivity;
import dz.lab.rottenpotatoes.view.MainView;
import dz.lab.rottenpotatoes.core.ActionBarModule;
import dz.lab.rottenpotatoes.core.ActionBarOwner;
import dz.lab.rottenpotatoes.core.ApplicationModule;
import dz.lab.rottenpotatoes.core.MainScope;
import flow.Backstack;
import flow.Flow;
import flow.HasParent;
import flow.Parcer;
import mortar.Blueprint;
import mortar.ViewPresenter;
import rx.functions.Action0;

public class Main implements Blueprint {
    @Override public String getMortarScopeName() {
        return getClass().getName();
    }

    @Override public Object getDaggerModule() {
        return new Module();
    }

    @dagger.Module(includes = ActionBarModule.class, injects = { MainActivity.class, MainView.class }, addsTo = ApplicationModule.class, library = true)
    public static class Module {
        @Provides
        @MainScope
        Flow provideFlow(Presenter presenter) {
            return presenter.getFlow();
        }
    }

    @Singleton
    public static class Presenter extends ViewPresenter<MainView> implements Flow.Listener {
        private static final String FLOW_KEY = "FLOW_KEY";

        private final ActionBarOwner actionBarOwner;
        private final Parcer<Object> parcer;
        private Flow flow;

        private final DateFormat format = new SimpleDateFormat();

        private int serial = -1;

        @Inject Presenter(Parcer<Object> flowParcer, ActionBarOwner actionBarOwner) {
            super();
            this.parcer = flowParcer;
            this.actionBarOwner = actionBarOwner;
        }

        @Override protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            if (flow == null) {
                Backstack backstack;

                if (savedInstanceState != null) {
                    backstack = Backstack.from(savedInstanceState.getParcelable(FLOW_KEY), parcer);
                } else {
                    backstack = Backstack.fromUpChain(getFirstScreen());
                }

                flow = new Flow(backstack, this);
            }

            //noinspection unchecked
            showScreen((Blueprint) flow.getBackstack().current().getScreen(), null);
        }
        //@Override
        public void showScreen(Blueprint newScreen, Flow.Direction direction) {
            boolean hasUp = newScreen instanceof HasParent;
            String title = newScreen.getClass().getSimpleName();
            /*ActionBarOwner.MenuAction menu = hasUp ? null: new ActionBarOwner.MenuAction("Friends", new Action0() {
                @Override
                public void call() {

                }
            });*/
            actionBarOwner.setConfig(new ActionBarOwner.Config(false, hasUp, title, new ArrayList<ActionBarOwner.MenuAction>()));
            //super.showScreen(newScreen, direction);
            MainView view = getView();
            if (view == null) return;

            view.showScreen(newScreen, direction);
        }

        @Override protected void onSave(Bundle outState) {
            super.onSave(outState);
            outState.putInt("serial", serial);
        }

        @Override
        public void go(Backstack nextBackstack, Flow.Direction direction, Flow.Callback callback) {
            Blueprint newScreen = (Blueprint) nextBackstack.current().getScreen();
            showScreen(newScreen, direction);
            callback.onComplete();
        }

        public final Flow getFlow() {
            return flow;
        }

        //@Override
        protected Blueprint getFirstScreen() {
            return new MovieListScreen();
        }
    }
}