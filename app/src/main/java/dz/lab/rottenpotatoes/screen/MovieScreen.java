package dz.lab.rottenpotatoes.screen;

import dz.lab.rottenpotatoes.R;
import android.os.Bundle;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Provides;
import dz.lab.rottenpotatoes.core.ActionBarOwner;
import dz.lab.rottenpotatoes.core.ActionBarOwner.Config;
import dz.lab.rottenpotatoes.core.MainScope;
import dz.lab.rottenpotatoes.model.Movie;
import dz.lab.rottenpotatoes.service.MoviesManager;
import dz.lab.rottenpotatoes.view.MovieView;
import flow.Flow;
import flow.HasParent;
import flow.Layout;
import mortar.Blueprint;
import mortar.ViewPresenter;

@Layout(R.layout.movie_view)
public class MovieScreen implements HasParent<MovieListScreen>, Blueprint {

    private final int movieIndex;

    public MovieScreen(int movieIndex) {
        this.movieIndex = movieIndex;
    }

    @Override
    public String getMortarScopeName() {
        return "MovieScreen{" + "movieIndex=" + movieIndex + '}';
    }

    @Override
    public Object getDaggerModule() {
        return new Module();
    }

    @Override
    public MovieListScreen getParent() {
        return new MovieListScreen();
    }

    @dagger.Module(injects = MovieView.class, addsTo = Main.Module.class)
    public class Module {
        @Provides
        Movie provideMovie(MoviesManager movies) {
            return movies.getMovie(movieIndex);
        }
    }

    @Singleton
    public static class Presenter extends ViewPresenter<MovieView> {
        private final Movie movie;
        private final Flow flow;
        private final ActionBarOwner actionBar;
        //private final PopupPresenter<Confirmation, Boolean> confirmer;

        @Inject
        public Presenter(Movie movie, @MainScope Flow flow, ActionBarOwner actionBar) {
            this.movie = movie;
            this.flow = flow;
            this.actionBar = actionBar;
            /*this.confirmer = new PopupPresenter<Confirmation, Boolean>() {
                @Override
                protected void onPopupResult(Boolean confirmed) {
                    if(confirmed)
                        Presenter.this.getView().toast("Haven't implemented that, friend.");
                }
            };*/
        }

        @Override
        public void dropView(MovieView view) {
            //TODO show confirmation popup
            //confirmer.dropView(view.getConfirmerPopup());
            super.dropView(view);
        }

        @Override
        public void onLoad(Bundle savedInstanceState) {
            MovieView v = getView();
            if(v == null) return;
            // configure the action bar
            CharSequence title = getView().getContext().getResources().getString(R.string.actionbar_title_movie_screen);
            Config config = actionBar.getConfig();
            Config newConfig = new Config(config.showHomeEnabled, config.upButtonEnabled, title, config.actions);
            actionBar.setConfig(newConfig);

            //TODO subscribe to receive movies
            v.show(movie);
        }

        @Override
        protected void onExitScope() {
            ensureStopped();
        }

        public void onConversationSelected(int position) {
            //TODO nothing
        }

        public void visibilityChanged(boolean visible) {
            if(!visible) {
                ensureStopped();
            }
        }

        private void ensureStopped() {
            //TODO stop listening to views
        }
    }
}

