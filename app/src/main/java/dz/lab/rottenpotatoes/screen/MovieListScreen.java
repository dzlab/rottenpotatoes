package dz.lab.rottenpotatoes.screen;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Provides;
import dz.lab.rottenpotatoes.R;
import dz.lab.rottenpotatoes.core.ActionBarOwner;
import dz.lab.rottenpotatoes.event.MovieEvent;
import dz.lab.rottenpotatoes.model.Movie;
import dz.lab.rottenpotatoes.service.MoviesManager;
import dz.lab.rottenpotatoes.view.MovieListView;
import flow.Flow;
import flow.Layout;
import mortar.Blueprint;
import mortar.ViewPresenter;
import rx.functions.Action0;

@Layout(R.layout.movie_list_view)
public class MovieListScreen implements Blueprint {
    private static final String TAG = "MovieListScreen";

    @Override
    public String getMortarScopeName() {
        return getClass().getName();
    }

    @Override
    public Object getDaggerModule() {
        return new Module();
    }

    @dagger.Module(injects = MovieListView.class, addsTo = Main.Module.class)
    static class Module {
        @Provides
        List<Movie> provideMovies(MoviesManager moviesManager) {
            return moviesManager.getAll();
        }
    }

    @Singleton
    public static class Presenter extends ViewPresenter<MovieListView> {
        private final Flow flow;
        private final List<Movie> movies;
        private final ActionBarOwner actionBar;

        @Inject public Bus bus;

        @Inject
        Presenter(Flow flow, List<Movie> movies, ActionBarOwner actionBar) {
            this.flow = flow;
            this.movies = movies;
            this.actionBar = actionBar;
        }

        @Override
        public void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            // configure the action bar
            Resources res = getView().getContext().getResources();
            CharSequence title = res.getString(R.string.actionbar_title_movie_list_screen);
            ActionBarOwner.Config config = actionBar.getConfig();
            //FIXME if config is null create a new one
            ActionBarOwner.MenuAction menuAction = new ActionBarOwner.MenuAction(
                    res.getString(R.string.actionbar_action_refresh),
                    R.drawable.ic_action_refresh,
                    new Action0() {
                        @Override
                        public void call() {
                            Log.d(TAG, "Refreshing the list of movies");
                        }
                    });
            ActionBarOwner.Config newConfig = new ActionBarOwner.Config(config.showHomeEnabled, config.upButtonEnabled, title, Arrays.asList(menuAction));
            actionBar.setConfig(newConfig);

            bus.register(this);
            refresh();
        }

        @Subscribe
        public void onRottenEvent(MovieEvent event){
            Log.d(TAG, "Received movies update event: " + event);
            movies.clear();
            movies.addAll(((MoviesManager)event.getSource()).getAll());
            refresh();
        }

        private void refresh() {
            MovieListView view = getView();
            if(view == null) return;
            view.showMovies(movies);
        }


        public void onMovieSelected(int position) {
            Log.d(TAG, "Selected movie at "+position+" position");
            bus.unregister(this);
            flow.goTo(new MovieScreen(position));
        }
    }

}

