package dz.lab.rottenpotatoes;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import java.util.List;

import javax.inject.Inject;

import dz.lab.rottenpotatoes.core.ActionBarOwner;
import dz.lab.rottenpotatoes.screen.Main;
import dz.lab.rottenpotatoes.view.MainView;
import flow.Flow;
import mortar.Mortar;
import mortar.MortarActivityScope;
import mortar.MortarScope;
import mortar.MortarScopeDevHelper;


public class MainActivity extends SherlockActivity implements ActionBarOwner.View {
    private static final String TAG = "MainActivity";

    private MortarActivityScope activityScope;
    private Flow mainFlow;
    private List<ActionBarOwner.MenuAction> menuActions;
    @Inject ActionBarOwner actionBarOwner;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MortarScope parentScope = ((RPApplication) getApplication()).getRootScope();
        activityScope = Mortar.requireActivityScope(parentScope, new Main());
        Mortar.inject(this, this);
        activityScope.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        MainView mainView = (MainView) findViewById(R.id.container);
        mainFlow = mainView.getFlow();

        actionBarOwner.takeView(this);
    }

    @Override public Object getSystemService(String name) {
        if (Mortar.isScopeSystemService(name)) {
            return activityScope;
        }
        return super.getSystemService(name);
    }

    @Override protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        activityScope.onSaveInstanceState(outState);
    }

    @Override protected void onDestroy() {
        super.onDestroy();

        if (isFinishing() && activityScope != null) {
            MortarScope parentScope = ((RPApplication) getApplication()).getRootScope();
            parentScope.destroyChild(activityScope);
            activityScope = null;
        }
    }

    /**
     * Inform the view about back events.
     */
    @Override public void onBackPressed() {
        // Give the view a chance to handle going back. If it declines the honor, let super do its thing.
        if (!mainFlow.goBack())
            super.onBackPressed();
    }

    /**
     * Inform the view about up events
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            return mainFlow.goUp();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Configure the action bar menu as required by {@link dz.lab.rottenpotatoes.core.ActionBarOwner.View}
     * @param menu
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        if(menuActions.isEmpty() == false) {
            for(final ActionBarOwner.MenuAction menuAction: menuActions)
            {
                MenuItem menuItem = menu.add(menuAction.title)
                        .setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS)
                        .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                menuAction.action.call();
                                return true;
                            }
                        });
                // set the icon if provided
                if(menuAction.icon != -1) {
                    menuItem.setIcon(menuAction.icon);
                }
            }
        }
        menu.add("Log Scope Hierarchy")
                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        Log.d(TAG, MortarScopeDevHelper.scopeHierarchyToString(activityScope));
                        return true;
                    }
                });
        return true;
    }

    @Override
    public void setShowHomeEnabled(boolean enabled) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public void setUpButtonEnabled(boolean enabled) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(enabled);
        actionBar.setHomeButtonEnabled(enabled);
    }

    @Override
    public void setTitle(CharSequence title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void setMenu(List<ActionBarOwner.MenuAction> actions) {
        if(actions != this.menuActions) {
            this.menuActions = actions;
            invalidateOptionsMenu();
        }
    }

    @Override
    public Context getMortarContext() {
        return this;
    }
}
