package dz.lab.rottenpotatoes.model;

/**
 * Created by Bachir on 26/10/2014.
 */
public class Movie {
    public String id;
    public String title;
    public int year;
    public String mpaa_rating;
    public int runtime;
    public String critics_consensus;
    public ReleaseDates release_dates;
    public Ratings ratings;
    public String synopsis;
    public Posters posters;
    private String imgUrl;

    /**
     * a class encapsulating information related to the movie release dates
     */
    public static class ReleaseDates {
        public String theater;
        public String dvd;
    }

    /**
     * a class encapsulating information related to the movie ratings
     */
    public static class Ratings {
        public String critics_rating;
        public int critics_score;
        public String audience_rating;
        public int audience_score;
    }

    /**
     * a class encapsulating information related to the movie posters
     */
    public static class Posters {
        /**
         * 61 x 91
         */
        public String thumbnail;
        /**
         * 120 x 178
         */
        public String profile;
        /**
         * 180 x 266
         */
        public String detailed;
        /**
         * 510 x 755
         */
        public String original;
    }

}
