package dz.lab.rottenpotatoes.model;

import java.util.List;

/**
 * Created by Bachir on 26/10/2014.
 */
public class Movies {

    private final List<Movie> movies;
    private final Links links;
    private final String link_template;

    public Movies(List<Movie> movies, Links links, String link_template) {
        this.movies = movies;
        this.links = links;
        this.link_template = link_template;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public static class Links {
        private final String self;
        private final String alternate;
        public Links(String self, String alternate) {
            this.self = self;
            this.alternate = alternate;
        }
    }
}
