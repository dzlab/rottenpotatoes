package dz.lab.rottenpotatoes.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import javax.inject.Inject;

import dz.lab.rottenpotatoes.screen.Main;
import dz.lab.rottenpotatoes.service.ScreenManager;
import flow.Flow;
import mortar.Blueprint;
import mortar.Mortar;

/**
 * Created by Bachir on 26/10/2014.
 */
public class MainView extends LinearLayout {
    @Inject Main.Presenter presenter;
    private final ScreenManager<Blueprint> screenMaestro;

    //private TextView textView;

    public MainView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Mortar.inject(context, this);
        screenMaestro = new ScreenManager<Blueprint>(context, this);
    }

    @Override protected void onFinishInflate() {
        super.onFinishInflate();
        //textView = (TextView) findViewById(R.id.text);
        presenter.takeView(this);
    }

    @Override protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        presenter.dropView(this);
    }

    public Flow getFlow() {
        return presenter.getFlow();
    }

    /*public void show(CharSequence stuff) {
        textView.setText(stuff);
    }*/
    //@Override
    public void showScreen(Blueprint screen, Flow.Direction direction) {
        screenMaestro.showScreen(screen, direction);
    }
}
