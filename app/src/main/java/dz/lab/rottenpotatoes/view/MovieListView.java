package dz.lab.rottenpotatoes.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewStub;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;

import com.etsy.android.grid.StaggeredGridView;
import java.util.List;

import javax.inject.Inject;

import dz.lab.rottenpotatoes.R;
import dz.lab.rottenpotatoes.adapter.MovieAdapter;
import dz.lab.rottenpotatoes.model.Movie;
import dz.lab.rottenpotatoes.screen.MovieListScreen;
import mortar.Mortar;

public class MovieListView extends FrameLayout implements AbsListView.OnScrollListener, AbsListView.OnItemClickListener {
    private static final String TAG = "MovieListView";

    @Inject
    MovieListScreen.Presenter presenter;

    private final MovieAdapter adapter;
    StaggeredGridView gridView;
    ViewStub emptyView;

    private boolean mHasRequestedMore;

    public MovieListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Mortar.inject(context, this);
        adapter = new MovieAdapter(context);
    }

    @Override
    protected void onFinishInflate() {
        Log.d(TAG, "Finishing view inflate.");
        super.onFinishInflate();
        emptyView = (ViewStub) findViewById(android.R.id.empty);
        gridView = (StaggeredGridView) findViewById(R.id.grid_view);
        gridView.setOnScrollListener(this);
        gridView.setOnItemClickListener(this);
        gridView.setAdapter(adapter);

        presenter.takeView(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        presenter.dropView(this);
    }

    public void showMovies(List<Movie> movies) {
        Log.d(TAG, "Showing "+movies.size()+" movie(s).");
        if(movies.isEmpty()) {
            emptyView.setVisibility(View.VISIBLE);
            gridView.setEmptyView(emptyView);
        } else {
            gridView.setEmptyView(null);
            emptyView.setVisibility(View.GONE);
            adapter.addAll(movies);
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int scrollState) {
        Log.d(TAG, "onScrollStateChanged:" + scrollState);
    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        Log.d(TAG, "onScroll firstVisibleItem:" + firstVisibleItem +
                " visibleItemCount:" + visibleItemCount +
                " totalItemCount:" + totalItemCount);
        // our handling
        if (!mHasRequestedMore) {
            int lastInScreen = firstVisibleItem + visibleItemCount;
            if (lastInScreen >= totalItemCount) {
                Log.d(TAG, "onScroll lastInScreen - so load more");
                mHasRequestedMore = true;
                onLoadMoreItems();
            }
        }
    }

    private void onLoadMoreItems() {
        /*final ArrayList<String> sampleData = SampleData.generateSampleData();
        for (String data : sampleData) {
            mAdapter.add(data);
        }*/
        // stash all the data in our backing store
        //mData.addAll(sampleData);
        // notify the adapter that we can update now
        adapter.notifyDataSetChanged();
        mHasRequestedMore = false;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        presenter.onMovieSelected(position);
    }
}
