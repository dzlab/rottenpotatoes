package dz.lab.rottenpotatoes.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import dz.lab.rottenpotatoes.R;
import dz.lab.rottenpotatoes.model.Movie;
import dz.lab.rottenpotatoes.screen.MovieScreen;
import dz.lab.rottenpotatoes.util.ImageChooser;
import mortar.Mortar;

public class MovieView extends LinearLayout {
    private static String TAG = "MovieView";
    private final ImageChooser imageChooser;
    @Inject
    MovieScreen.Presenter presenter;

    private ImageView profile;
    private TextView title;
    private TextView description;

    //private final ConfirmerPopup confirmerPopup;

    public MovieView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        Mortar.inject(context, this);
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        this.imageChooser = new ImageChooser(metrics.densityDpi);
        //confirmerPopup = new ConfirmerPopup(context);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        profile = (ImageView) findViewById(R.id.profile);
        title = (TextView) findViewById(R.id.title);
        description = (TextView) findViewById(R.id.description);
        presenter.takeView(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        presenter.dropView(this);
    }

    @Override
    protected void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
        presenter.visibilityChanged(visibility == VISIBLE);
    }

    /*public ConfirmerPopup getConfirmerPopup() {
        return confirmerPopup;
    }*/

    public void toast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    public void show(Movie movie) {
        Log.d(TAG, "Showing movie: "+movie);
        String imageUrl = imageChooser.getDetailedUrl(movie);
        Picasso.with(getContext()).load(imageUrl).into(profile);
        title.setText(movie.title);
        description.setText(movie.critics_consensus);
    }
}

