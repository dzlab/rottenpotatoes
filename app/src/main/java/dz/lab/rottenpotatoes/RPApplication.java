package dz.lab.rottenpotatoes;

import android.app.Application;

import dagger.ObjectGraph;
import dz.lab.rottenpotatoes.core.ApplicationModule;
import mortar.Mortar;
import mortar.MortarScope;

public class RPApplication extends Application {
    private MortarScope rootScope;

    @Override public void onCreate() {
        super.onCreate();
        //rootScope = Mortar.createRootScope(BuildConfig.DEBUG);
        rootScope = Mortar.createRootScope(BuildConfig.DEBUG, ObjectGraph.create(new ApplicationModule(this)));
    }

    public MortarScope getRootScope() {
        return rootScope;
    }
}