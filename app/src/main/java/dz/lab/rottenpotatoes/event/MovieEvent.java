package dz.lab.rottenpotatoes.event;

/**
 * Created by Bachir on 26/10/2014.
 */
public class MovieEvent {
    private final Object source;
    public MovieEvent(Object source) {this.source = source;}

    public Object getSource() {
        return source;
    }
}
