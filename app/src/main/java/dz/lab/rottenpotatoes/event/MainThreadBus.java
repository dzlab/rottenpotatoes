package dz.lab.rottenpotatoes.event;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;


/**
 * see <a href="http://stackoverflow.com/questions/15431768/how-to-send-event-from-service-to-activity-with-otto-event-bus">Posting to the main thread</a>
 */
public class MainThreadBus extends Bus {
    private final Handler handler;

    public MainThreadBus() {
        super();
        handler = new Handler(Looper.getMainLooper());
    }

    @Override public void register(Object obj) {
        super.register(obj);
    }

    @Override public void unregister(Object obj) {
        super.unregister(obj);
    }

    @Override public void post(final Object event) {
        if(Looper.myLooper() == Looper.getMainLooper()) {
            super.post(event);
        }else {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    MainThreadBus.super.post(event);
                }
            });
        }
    }
}
