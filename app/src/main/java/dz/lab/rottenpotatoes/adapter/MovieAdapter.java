package dz.lab.rottenpotatoes.adapter;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import dz.lab.rottenpotatoes.R;
import dz.lab.rottenpotatoes.model.Movie;
import dz.lab.rottenpotatoes.util.ImageChooser;

/**
 * Adapter for movies
 */
public class MovieAdapter extends BaseAdapter {
    private static final String TAG = "MovieAdapter";

    private final Context context;
    private final LayoutInflater inflater;
    private final List<Movie> movies;
    private final ImageChooser imageChooser;

    public MovieAdapter(Context context) {
        this.context = context;
        this.movies = new ArrayList<Movie>();
        this.inflater = LayoutInflater.from(context);
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        this.imageChooser = new ImageChooser(metrics.densityDpi);
    }

    public void addAll(Collection<Movie> movies) {
        this.movies.addAll(movies);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return movies.size();
    }

    @Override
    public Object getItem(int i) {
        return movies.get(i);
    }

    @Override
    public long getItemId(int i) {
        return Long.valueOf(movies.get(i).id);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.movie_list_item_view, null);
            holder = new ViewHolder();
            holder.thumbnail = (ImageView) convertView .findViewById(R.id.movie_thumbnail);
            holder.title  = (TextView) convertView .findViewById(R.id.movie_title);
            holder.year = (TextView) convertView .findViewById(R.id.movie_year);
            holder.rating  = (TextView) convertView .findViewById(R.id.movie_rating);
            convertView.setTag(holder);
        }

        holder = (ViewHolder) convertView.getTag();
        Movie movie = (Movie) getItem(position);
        String imageUrl = imageChooser.getThumbnailUrl(movie);
        Picasso.with(context).load(imageUrl).into(holder.thumbnail);
        holder.title.setText(movie.title);
        holder.year.setText(String.valueOf(movie.year));
        holder.rating.setText(String.valueOf(movie.ratings.audience_score));

            /*if(position == getCount()-1) //load more feeds
                new FeedsTask().execute();*/
        return convertView;
    }

    private static class ViewHolder {
        ImageView thumbnail;
        TextView title;
        TextView year;
        TextView rating;
    }

}
