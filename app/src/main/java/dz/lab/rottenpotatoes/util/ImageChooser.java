package dz.lab.rottenpotatoes.util;

import android.content.Context;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import dz.lab.rottenpotatoes.model.Movie;

/**
 * Created by Bachir on 30/10/2014.
 */
public class ImageChooser {

    private final int densityDpi;

    public ImageChooser(int densityDpi) {
        this.densityDpi = densityDpi;
    }

    public String getThumbnailUrl(Movie movie) {
        String imageUrl = movie.posters.thumbnail;
        switch (densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
            case DisplayMetrics.DENSITY_MEDIUM:
                imageUrl = movie.posters.thumbnail;
                break;

            case DisplayMetrics.DENSITY_HIGH:
            case DisplayMetrics.DENSITY_XHIGH:
                imageUrl = movie.posters.profile;
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                imageUrl = movie.posters.detailed;
                break;
        }
        return imageUrl;
    }

    public String getDetailedUrl(Movie movie) {
        String imageUrl = movie.posters.profile;
        switch (densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
            case DisplayMetrics.DENSITY_MEDIUM:
                imageUrl = movie.posters.profile;
                break;

            case DisplayMetrics.DENSITY_HIGH:
            case DisplayMetrics.DENSITY_XHIGH:
                imageUrl = movie.posters.detailed;
                break;

            case DisplayMetrics.DENSITY_XXHIGH:
                imageUrl = movie.posters.original;
                break;
        }
        return imageUrl;
    }

    /**
     * Get the size of the screen
     * @param context
     * @return
     */
    public static Point getScreenSize(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point dimension = new Point();
        display.getSize(dimension);
        return dimension;
    }



}
